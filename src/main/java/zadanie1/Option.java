package zadanie1;

public enum Option {
  EXIT(0, "EXIT"),
  CREATE_TABLE(1, "CREATE TABLE"),
  INSERT_TABLE(2, "INSERT TABLE"),
  UPDATE_TABLE(3, "UPDATE TABLE"),
  SHOW_TABLE(4, "SHOW TABLE"),
  DELETE_FROM_TABLE(5, "DELETE FROM TABLE");


  int value;
  String description;

  Option(int value, String description) {
    this.value = value;
    this.description = description;
  }

  public int getValue() {
    return value;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return value + "-->" + description;
  }

  static Option createFromInt(int option) throws ArrayIndexOutOfBoundsException {
    if (option < Option.values().length) {
      return Option.values()[option];
    } else {
      throw new ArrayIndexOutOfBoundsException();
    }

  }
}
