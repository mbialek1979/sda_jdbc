package zadanie1;

import static zadanie1.EmployeeRepository.scanner;

import java.util.InputMismatchException;

public class EmployeeMenu {

  public void dbMenu() {

    Option option = null;
    EmployeeRepository employeeRepository = new EmployeeRepository();

    do {
      printOption();
      boolean error = true;
      do {
        try {
          option = Option.createFromInt(getInt());
          error = false;
        } catch (ArrayIndexOutOfBoundsException e) {
          System.err.println("This option doesn't exist. Choose correct!!!");
        }
      } while (error);

      switch (option) {
        case EXIT: {
          System.out.println("EXIT PROGRAM BYE BYE !!!");
          scannerClose();
          break;
        }
        case CREATE_TABLE: {
          employeeRepository.createTable("employee");
          break;
        }
        case INSERT_TABLE: {
          employeeRepository.insertTable(employeeRepository.createEmployee());
          break;

        }
        case UPDATE_TABLE: {
          employeeRepository.updateTable();
          break;
        }
        case DELETE_FROM_TABLE: {
          employeeRepository.deleteFromTable();
          break;
        }
        case SHOW_TABLE:
          employeeRepository.selectAllTable();
          break;
      }

    } while (option != Option.EXIT);

  }

  private void printOption() {
    System.out.println("Choice option: ");
    for (Option option : Option.values()
    ) {
      System.out.println(option);

    }
  }

  private void scannerClose() {
    scanner.close();
  }


  public static int getInt() {
    boolean error = true;
    int number = 0;
    do {
      try {
        number = scanner.nextInt();
        error = false;
      } catch (InputMismatchException ex) {
        System.out.println("You didn't choose number, try again..");
      } finally {
        scanner.nextLine();
      }

    } while (error);
    return number;
  }


}
