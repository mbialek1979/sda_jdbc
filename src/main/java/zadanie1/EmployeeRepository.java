package zadanie1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class EmployeeRepository {

//  String url = "jdbc:mysql://localhost:3306/company?serverTimezone=UTC";
//  String user = "root";
//  String password = "rootroot";

  public static Scanner scanner = new Scanner(System.in);

  Connection myConn = null;


  public Employee createEmployee() {

    String firstName = null;
    String lastName = null;
    int salary = 0;
    String dateFormat;
    LocalDate dateOfEmployment = null;
    Employee employee = null;
    boolean error = true;

    do {
      try {
        System.out.println("Enter first name:");
        firstName = scanner.next();
        System.out.println("Enter last name:");
        lastName = scanner.next();
        System.out.println("Enter salary (\"INT\"):");
        salary = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Enter date of employment (\"yyyy-MM-dd\"):");
        dateFormat = scanner.next();
        dateOfEmployment = LocalDate.parse(dateFormat);
        error = false;
      } catch (InputMismatchException e) {
        System.err.println("Entered incorrect data. Try again");
      } catch (DateTimeParseException e) {
        System.err.println("Entered wrong date format. Try again");
      } finally {
        scanner.nextLine();
      }
    } while (error);

    employee = new Employee(firstName, lastName, salary, dateOfEmployment);
    return employee;
  }

  //=========================

//  public Connection connect(String url, String user, String password) {
//    Connection myConn = null;
//    try {
//      // Get a connection to database
//
//      myConn = DriverManager.getConnection(url, user, password);
//      System.out.println("Connection to database" + " " + url + " -----> OK!!!");
//
//    } catch (Exception e) {
//      System.err.println("Connection to database" + " " + url + " -----> FAILED!!!\n" +
//          e.getMessage());
//      return null;
//    }
//    return myConn;
//
//  }

  //=========================

  public void createTable(String tableName){

    // Connection myConn = connect(url, user, password);
    // Statement myStmt = null;
    try {

      myConn = DbUtil.getInstance().getConnection();
      //Wyłączamy tryb autocommit i rozpoczynamy transakcję
      myConn.setAutoCommit(false);

      //myStmt = myConn.createStatement();
      Statement myStmt = myConn.createStatement();

      //SQL command to create a table;
      String commandSQL = "CREATE TABLE " + tableName
          + " (ID INT   PRIMARY KEY   NOT NULL    AUTO_INCREMENT,"
          + " firstName         CHAR(50)    NOT NULL, "
          + " lastName          CHAR(50)    NOT NULL, "
          + " salary            INT         NOT NULL, "
          + " dateOfEmployment  DATE        NOT NULL  )";

      myStmt.executeUpdate(commandSQL);
      System.out.println("Table creation" + " " + tableName + " -----> OK!!!");
      //myStmt.close();
      //myConn.close();
      myConn.commit();
    } catch (SQLException e) {
      try {
        myConn.rollback();
      } catch (SQLException ex) {
        ex.printStackTrace();
      }
      System.err.println("Table creation" + " " + tableName + " -----> FAILED!!!\n" +
          e.getMessage());

    }
  }
  //=========================

  public void insertTable(Employee employee) {

    //  Connection myConn = connect(url, user, password);
    //  Statement myStmt = null;

    try {
      myConn = DbUtil.getInstance().getConnection();
      Statement myStmt = myConn.createStatement();
      String commandSQL =
          "INSERT INTO employee" + " (firstName, lastName, salary, dateOfEmployment)"
              + " VALUES ("
              + "'" + employee.getFirstName() + "',"
              + "'" + employee.getLastName() + "',"
              + employee.getSalary() + ","
              + "'" + employee.getDateOfEmployment() + "'"
              + ");";
      myStmt.executeUpdate(commandSQL);
      System.out.println("Insert to table -----> OK!!!");
      //myStmt.close();
      //myConn.close();
      myConn.commit();
    } catch (SQLException e) {
      try {
        myConn.rollback();
      } catch (SQLException ex) {
        ex.printStackTrace();
      }
      System.err.println("Insert to table -----> FAILED!!!\n" +
          e.getMessage());


    }
  }

  //=========================

  public void updateTable(){

    System.out.println("Enter ID record You want to update");
    int ID = EmployeeMenu.getInt();
    System.out.println("Enter new salary value");
    int salary = EmployeeMenu.getInt();

    //Connection myConn = connect(url, user, password);
    //PreparedStatement myPrStmt;

    String commandSQL = "UPDATE employee SET salary = ?  WHERE ID = ?";

    try {
      myConn = DbUtil.getInstance().getConnection();
      PreparedStatement myPrStmt = myConn.prepareStatement(commandSQL);
      myPrStmt.setInt(2, ID);
      myPrStmt.setInt(1, salary);
      int result = myPrStmt.executeUpdate();
      if (result == 1) {
        System.out.println("Insert to table -----> OK!!!");
      } else {
        System.out.println("You entered wrong ID number, Insert to table -----> NO EFFECT!!!");
      }
      myConn.commit();
      //myPrStmt.close();
      //myConn.close();

    } catch (SQLException e) {
      try {
        myConn.rollback();
      } catch (SQLException ex) {
        ex.printStackTrace();
      }
      System.err.println("Insert to table -----> FAILED!!!\n" +
          e.getMessage());

    }
  }

  public void deleteFromTable() {

    System.out.println("Enter ID record You want to delete");
    int ID = EmployeeMenu.getInt();

    // Connection myConn = connect(url, user, password);
    //PreparedStatement myPrStmt;

    String commandSQL = "  DELETE FROM employee WHERE ID = ?";

    try {
      myConn = DbUtil.getInstance().getConnection();
      PreparedStatement myPrStmt = myConn.prepareStatement(commandSQL);
      myPrStmt.setInt(1, ID);
      int result = myPrStmt.executeUpdate();
      if (result == 1) {
        System.out.println("Delete from table -----> OK!!!");
      } else {
        System.out.println("You entered wrong ID number, Delete from table -----> NO EFFECT!!!");
      }
      //myPrStmt.close();
      //myConn.close();
      myConn.commit();

    } catch (SQLException e) {
      try {
        myConn.rollback();
      } catch (SQLException ex) {
        ex.printStackTrace();
      }
      System.err.println("Delete from table -----> FAILED!!!\n" +
          e.getMessage());

    }

    //=========================

  }

  public void selectAllTable() {

    //Connection myConn = connect(url, user, password);
    //Statement myStmt = null;

    try {
      myConn = DbUtil.getInstance().getConnection();
      Statement myStmt = myConn.createStatement();
      String commandSQL = "SELECT * from employee";
      ResultSet rs = myStmt.executeQuery(commandSQL);
      Employee employee = new Employee();

      while (rs.next()) {
        employee.setFirstName(rs.getString("firstName"));
        employee.setLastName(rs.getString("lastName"));
        employee.setSalary(rs.getInt("salary"));
        employee.setDateOfEmployment(rs.getDate("dateOfEmployment").toLocalDate());
        System.out.println(employee);
      }

      System.out.println("Show all positions from a table -----> OK!!!");
      //myStmt.close();
      //myConn.close();
      myConn.commit();
    } catch (SQLException e) {
      try {
        myConn.rollback();
      } catch (SQLException ex) {
        ex.printStackTrace();
      }
      System.err.println("Show all positions from a table -----> FAILED!!!\n" +
          e.getMessage());

    }
  }
}
