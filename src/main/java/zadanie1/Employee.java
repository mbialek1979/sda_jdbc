package zadanie1;

import java.time.LocalDate;
import java.util.Date;

public class Employee {

  String firstName;
  String lastName;
  int salary;
  LocalDate dateOfEmployment;

  public Employee(String firstName, String lastName, int salary, LocalDate dateOfEmployment) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.salary = salary;
    this.dateOfEmployment = dateOfEmployment;
  }

  public Employee(){}

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getSalary() {
    return salary;
  }

  public void setSalary(int salary) {
    this.salary = salary;
  }

  public LocalDate getDateOfEmployment() {
    return dateOfEmployment;
  }

  public void setDateOfEmployment(LocalDate dateOfEmployment) {
    this.dateOfEmployment = dateOfEmployment;
  }


  @Override
  public String toString() {
    return "Employee{" +
        "firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", salary=" + salary +
        ", dateOfEmployment=" + dateOfEmployment +
        '}';
  }
}
