package zadanie1;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.sql.*;

public class DbUtil {

  private static DbUtil dbUtil;

  // obiekt C3P0 - zarządza połaczeniami do bazy danych
  private ComboPooledDataSource connectionPool;

  private DbUtil() throws PropertyVetoException {
    connectionPool = new ComboPooledDataSource();

    // PODSTAWOWE INFORMACJE WYMAGANE DO POŁĄCZENIA Z BAZĄ DANYCH
    // connectionPool.setDriverClass("com.mysql.cj.jdbc.Driver");

    connectionPool.setJdbcUrl("jdbc:mysql://localhost:3306/company?serverTimezone=UTC");
    connectionPool.setUser("root");
    connectionPool.setPassword("rootroot");

    // KONFIGURACJA PULI POŁĄCZEŃ:

    //początkowa liczba połaczeń
    connectionPool.setInitialPoolSize(2);

    // min. dostępna liczba podtrzymywanych połączeń
    connectionPool.setMinPoolSize(2);

    // max liczba podtrzymywanych połączeń
    connectionPool.setMaxPoolSize(10);

    // ilość dodatkowych połczeń, która ma zostać otworzona, gdy wszystkie są zajęte
    connectionPool.setAcquireIncrement(2);

    // maxymalny czas podtrzymywania połczenia w sekundach
    connectionPool.setMaxIdleTime(600);
  }

  public Connection getConnection() throws SQLException {
    return connectionPool.getConnection();
  }

  //implementujemy Singleton

  public static DbUtil getInstance() {
    if (dbUtil == null) {
      try {
        dbUtil = new DbUtil();
      } catch (PropertyVetoException e) {
        e.printStackTrace();
      }
    }
    return dbUtil;
  }


}


