package JDBCTutorial.e1Connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {

  public static void main(String[] args) {

    try {

      // 1. Get a connection to database
      Connection myConn = DriverManager // ?serverTimezone=UTC
          .getConnection("jdbc:mysql://localhost:3306/demo?serverTimezone=UTC", "root", "rootroot");

      // 2. Create a statement
      Statement myStmt = myConn.createStatement();

      // 3. Execute SQL query
      ResultSet myRS = myStmt.executeQuery("select * from employees");

      // 4. Process the result set
      while (myRS.next()) {
        System.out.println(myRS.getString("first_name") + " , " + myRS.getString("last_name"));

      }

    } catch (Exception exc) {
      exc.printStackTrace();
    }

  }
}
